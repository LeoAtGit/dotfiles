" Make vim use the mouse
if has('mouse')
  set mouse=a
endif

" Only use 16 colors
set t_Co=16

" syntax highlighting
syntax enable

" display a column at 80 characters
set colorcolumn=81

set nocompatible

" Go searching for tags one directory up until it finds a tags file
set tags=tags;/
 
" Make backspace behave in a sane manner.
set backspace=indent,eol,start

" For highlighted search results
set hlsearch 
 
" Always use C indentation
set cindent
 
" Show line numbers and make them relative to the cursor
set number
set relativenumber
 
" Allow hidden buffers, don't limit to 1 file per window/split
set hidden

" keep 50 lines of command line history
set history=50

" show the cursor position all the time
set ruler

" display incomplete commands
set showcmd

" disable the arrow keys
""inoremap <Up> <NOP>
""inoremap <Down> <NOP>
""inoremap <Right> <NOP>
""inoremap <Left> <NOP>
""noremap <Up> <NOP>
""noremap <Down> <NOP>
""noremap <Right> <NOP>
""noremap <Left> <NOP>

