#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# for the nice colors and asthetics
alias ls='ls --color=auto'
#PS1='\W $ '

export EDITOR='vim'
#export PATH=/sbin:/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_:/home/leo/devel/Dropbox/scripts:/usr/bin/core_perl:/home/leo/bin

# aliases for virtual machines
alias qemu_scull_drivers='qemu-system-x86_64 -cpu Haswell -smp cores=2 -m 2G -k de -redir tcp:2222::22 modules_arch.img -enable-kvm'
alias qemu_fedora='qemu-system-x86_64 -enable-kvm -m 4G -drive file=fedoradisk.img,if=virtio -cpu host -smp 2 -vga qxl -soundhw all -show-cursor'
alias qemu_webdev='qemu-system-x86_64 -enable-kvm -m 2G -drive file=webdev.img,if=virtio -cpu host -smp 2 -vga qxl -redir tcp:2222::22 -redir tcp:8080::80'
alias qemu_arch="qemu-system-x86_64 -cpu host -smp cores=2 -m 3G -enable-kvm -vga qxl -hda archlinux.img -soundhw all"
alias qemu_debian="qemu-system-x86_64 -cpu host -smp cores=2 -m 3G -enable-kvm -vga qxl -hda debian_harddisk_clean.img -soundhw all"
alias qemu_windows="qemu-system-x86_64 -enable-kvm -m 3G -cpu host -smp cores=2 -vga qxl -soundhw all -hda windows_harddisk_clean.img"

alias ssh_web='ssh -p 2222 server@localhost -c aes128-ctr'

# this is needed so that we can copy and paste in xterm
alias xterm="xterm -ls -xrm 'XTerm*selectToClipboard: true'&"

# set colemak as layout but only if X is running
if [ $(ps -Cstartx | wc -l) -gt 1 ]; then
	setxkbmap -layout us -variant colemak
fi

# so we don't get that ugly ssh login mask for git
unset SSH_ASKPASS

# function that finds .c and .h files in a dir-tree
function findc ()
{
	find $1 -name "*.[ch]"
}

PS1="\[\033[0;31m\]\W $\[\033[0m\] "

alias xbox_controller="sudo xboxdrv --silent --detach-kernel-driver --trigger-as-button --ui-axismap x2=ABS_Z,y2=ABS_RZ --ui-buttonmap A=BTN_B,B=BTN_X,X=BTN_A,TR=BTN_THUMBL,TL=BTN_MODE,GUIDE=BTN_THUMBR"

