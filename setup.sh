#!/bin/bash

# This script will copy all of my dotfiles and all of my other configuration 
# files to the directorys they belong to. 
# It includes:
# 	- i3 config
# 	- .bashrc
# 	- .vimrc
#
# More may follow. This script will copy those files to the locations where they
# belong to on Arch Linux. These locations can vary on different distros.

home_dir=~/

##################
# Initialization #
##################

echo
echo "This script assumes that this directory is your home directory."
echo $home_dir
echo "If this is your home dir type y, else n"
read answer
if [ "$answer" != "y" ]; then
	echo "todo"
fi
echo
echo "If there are already existing config files, the old ones will be renamed"
echo "to .[old_filename].old"
echo

#############
# Functions #
#############

function copy_to_home {
	if [ -f $1 ]; then
		if [ -f "$home_dir"$1 ]; then
			echo "Making a copy of your old $1..."
			mv "$home_dir"$1 "$home_dir"$1.old  
		fi

		cp $1 "$home_dir"$1 
		echo "New $1 now deployed"
	else
		echo "No copiable $1 found. Are you in the correct directory?"
	fi
}

###########
# .bashrc #
###########

copy_to_home ".bashrc"

##########
# .vimrc #
##########

copy_to_home ".vimrc"

##################
# .i3status.conf #
##################

copy_to_home ".i3status.conf"

###########
# .muttrc #
###########

copy_to_home ".muttrc"

#################
# .bash_profile #
#################

copy_to_home ".bash_profile"

###############
# .Xresources #
###############

copy_to_home ".Xresources"

#############
# .asoundrc #
#############

copy_to_home ".asoundrc"

##############
# .gitconfig #
##############

copy_to_home ".gitconfig"

#############
# i3 config #
#############

if [ -f config-i3 ]; then
	if [ ! -d "$home_dir".i3 ]; then
		echo "Creating a .i3 dir in your home directory"
		mkdir "$home_dir".i3 
	fi

	if [ -f "$home_dir".i3/config ]; then
		echo "Making a copy of your old i3 config..."
		mv "$home_dir".i3/config "$home_dir".i3/config.old  
	fi

	cp config-i3 "$home_dir".i3/config 
	echo "New i3 cofig now deployed"
else
	echo "No config file for i3 found in this directory!"
fi

################
# compton.conf #
################

copy_to_home "compton.conf"

############
# .xinitrc #
############

copy_to_home ".xinitrc"

exit 0
