#!/bin/bash

# Copies the configuration files of the user to this directory
# Asks to overwrite stuff

dotfiles_dir=~/dotfiles/

echo "This script will copy the config files to this directory:"
echo $dotfiles_dir
echo "If this is correct, type y, otherwise n"
read answer
if [ "$answer" != "y" ]; then
	echo "todo"
fi
echo "This script will overwrite any files in the dotfiles directory!"

if [ ! -d "$dotfiles_dir" ]; then
	echo "The specified directory does not exist. Please first git clone the master dir"
fi

#############
# Functions #
#############

# $1 = path/to/original_filename
# $2 = how it shall be saved

function copy_to_dotfiles {
	if [ -f "$1" ]; then
		if [ -f "$dotfiles_dir"$2 ]; then
			difference="$(diff "$1" "$2")"
			if [ -n "$difference" ]; then
				echo "This file $1 already exists, and they differ. Do you want to overwrite it?"
				echo "Here are the differences:"
				echo
				echo "$difference"
				echo
				echo
				echo "Do you want to overwrite?"
				read answer
				if [ "$answer" == "y" ]; then
					cp -v "$1" "$dotfiles_dir"$2
					echo "Copied the file $1"
				else
					echo "Didn't copy anything"
				fi
			else
				echo "Skipping $1 because no changes..."
			fi
		else
			cp -v "$1" "$dotfiles_dir"$2
			echo "Copied the file $1"
		fi
	else
		echo "Didn't find that file $1"
	fi
}

copy_to_dotfiles ~/.config/i3/i3blocks.conf

##############
# .tmux.conf #
##############

copy_to_dotfiles ~/.tmux.conf .tmux.conf

###########
# .bashrc #
###########

copy_to_dotfiles ~/.bashrc .bashrc

##########
# .vimrc #
##########

copy_to_dotfiles ~/.vimrc .vimrc

##################
# .i3status.conf #
##################

copy_to_dotfiles ~/.i3status.conf .i3status.conf

###########
# .muttrc #
###########

copy_to_dotfiles ~/.muttrc .muttrc

#################
# .bash_profile #
#################

copy_to_dotfiles ~/.bash_profile .bash_profile

###############
# .Xresources #
###############

copy_to_dotfiles ~/.Xresources .Xresources

#############
# .asoundrc #
#############

copy_to_dotfiles ~/.asoundrc .asoundrc

##############
# .gitconfig #
##############

copy_to_dotfiles ~/.gitconfig .gitconfig

##############
# .i3/config #
##############

#copy_to_dotfiles ~/.i3/config config-i3
# diffenrent linux distros, different places
copy_to_dotfiles ~/.config/i3/config config-i3

################
# compton.conf #
################

copy_to_dotfiles ~/compton.conf compton.conf

############
# .xinitrc #
############

copy_to_dotfiles ~/.xinitrc .xinitrc

exit 0

